const { app, BrowserWindow, ipcMain, dialog } = require('electron')
const path = require('path')

var window = null
function createMainWindow () {
    window = new BrowserWindow({
      width: 1000,
      height: 600,
      webPreferences: {
        preload: path.join(__dirname, '/scripts/preload.js')
      },

    })
  
    window.loadFile('./layouts/index.html')
  }


  var optionsWindow = null
  function createOptionsWindow() {
    optionsWindow = new BrowserWindow({
      width: 600,
      height: 400,
      resizable: true,
      parent: window,
      modal: true,
      webPreferences: {
        preload: path.join(__dirname, '/scripts/preload-options.js')
      },
    })
    optionsWindow.loadFile("./layouts/options.html")
    optionsWindow.on("close", function(event) {
      event.preventDefault()
      optionsWindow.hide()
      /*
      window.webContents.send() here to request all selected arguments maybe?
      */
    })
    optionsWindow.hide()
  }

  app.whenReady().then(() => {
    createMainWindow()
    createOptionsWindow()
  })
  


var spawn = require('child_process').spawn;

ipcMain.on('execute', (event, fileObject) => {
  console.log(fileObject)
  for(let i = 0;i < fileObject._files.length; i++){
    var cmd = "ffmpeg"
    var args = constructArgs(fileObject._files[i], fileObject._outputDir)
    /*
    var proc = spawn(cmd, args)
    window.webContents.send("conversion-started", i)
    proc.stderr.setEncoding("utf8")
    proc.stderr.on('data', function(data) {
        window.webContents.send("status-update", i, data)
    });
    
    proc.on('close', function() {
        window.webContents.send("conversion-complete", i)
    });*/
    } 
})


/*
This function should probably be responsible for fetching necessary data from optionsWindow
*/
function constructArgs(file, outputDir) {
    var input = file.path
    var output = outputDir + file.name + ".m4a" 
    var args = [
      "-loglevel", "info",
      "-y", //overwrite existing files so I don't have delete before every test
      "-r", "25", 
      "-i", input,
      "-vcodec", "copy",
      "-acodec", "alac",
      output,
    ]
    return args
}



ipcMain.on("select-folder", (event) => {
  dialog.showOpenDialog(window, {
    properties: ['openDirectory']
  }).then(path => {
    event.reply("folder-selected", path.filePaths + "\\")
  })
})






ipcMain.on("open-options-modal", (event) => {
  console.log("opening options modal")
  if (optionsWindow) {
    optionsWindow.show()
    optionsWindow.focus()
    return
  }  
})

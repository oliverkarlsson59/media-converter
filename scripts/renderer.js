
/*
Extend class with functionality for output file type
And UI elements that set different arg values
*/
class myObject {
    constructor(fileArr = [], argsArr = [], outputDir = null) {
        this._files = fileArr //Array of objects that state filename, filetype and filepath
        this._args = argsArr, /*array of objects that set ffmpeg args
        will probably need to be divided into two different arrays, one with arguments
        before input file, and another with arguments that should come after
        makes parsingand creating the runnable command much simpler
        */
        this._outputDir = outputDir //string
    }

    get files() {
        return this._files
    }

    get args() {
        return this._args
    }

    get outputDir() {
        return this._outputDir
    }

    set files(fileArr) {
        this._files = fileArr
    }

    set args(argsArr) {
        this._args = argsArr
    }

    set outputDir(dxDir) {
        this._outputDir = dxDir
    }

    addFile(file) {
        let extension = file.type.split('/')[1]
        if(extension == "mpeg"){
            extension = "mp3"
        }
        let filename = file.name.split('.' + extension)[0]
        let filepath = file.path
        this._files.push({name: filename, "type": extension, "path": filepath})
    }

    clearFilesList() {
        this._files = []
    }

    deleteFileItem(index) {
        this._files.splice(index, 1)
    }

    /*
    Todo:
    Not sure where to add it yet but there needs to be a function that correctly
    constructs the arguments for the ffmpeg command that runs in main.js.
    Probably best to let the back-end create it.
    */

}

/*
Central object that gets manipulated by the front-end
*/
var cmdObj = new myObject()



var convertButton = document.getElementById("run-conversion-button")
convertButton.addEventListener("click", function(){

    /*
    Haven't found a better place to assign outputDir yet.
    Probably needs to be set and validated each time the element changes
    But haven't made it work just yet so this is a temporary solution
    */
    cmdObj._outputDir = outputDirField.value
    window.api.send("execute", cmdObj)
})


/*
reset values of elements & variables containing data about previously selected files
*/
var clearListButton = document.getElementById("clear-list-button")
clearListButton.addEventListener("click", function(){
    cmdObj.clearFilesList()
    document.getElementById("selected-files-list").innerHTML = ""
    document.getElementById("file-selector").value = null
})

/*
loop through each selected file and create HTML elements for them
then add correct onclick reactions & push the file to the cmdObj
*/
var fileSelector = document.getElementById("file-selector")
fileSelector.addEventListener("change", function() {
    var files = Array.from(fileSelector.files)
    const fileList = document.getElementById("selected-files-list")
    files.forEach(function(file, index) {
        /*
        This entire string needs to be concatenated as seen below.
        Every added linebreak or space will end up as a child element of type text
        and therefore break the parsing that finds these elements
        */
        fileList.insertAdjacentHTML("beforeend",
        `<li class='file-item'><span class='file-item-header'><text>${file.name}</text><span class='delete-list-item-button' onclick='deleteListItem(${index})'>X</span></span><span class='file-status-text'>Ready</span></li>`)
        cmdObj.addFile(file)
    })
})


/*
This is made an entirely separate function so that the cmdObj
does not need to know about which front-end elements to manipulate
*/
function deleteListItem(i) {
    cmdObj.deleteFileItem(i)
    var list = document.getElementById("selected-files-list")
    list.removeChild(list.childNodes[i])
    /*
    Loop through all child elements and clone them to remove previous event listeners
    assign .onclick = null in case it is an origin element
    then add a new event listener with the correct index in function parameter
    Probably not the most performant solution but it does what it is supposed to
    */
    list.childNodes.forEach((element, index) => {
        var newElement = element.childNodes[0].childNodes[1].cloneNode(true)
        element.childNodes[0].childNodes[1].replaceWith(newElement)
        newElement.onclick = null
        newElement.addEventListener("click", function(){
            deleteListItem(index)
        })
    })    
}


var selectFolderButton = document.getElementById("select-folder-button").addEventListener("click", function(){
    window.api.send("select-folder")
})


/*
Not working as intended just yet.
*/
var outputDirField = document.getElementById("output-destination")
outputDirField.addEventListener("change", function(){
    cmdObj._outputDir = outputDirField.value
})

var optionsMenuButton = document.getElementById("options-button")
optionsMenuButton.addEventListener("click", function() {
    window.api.send("open-options-modal")
})


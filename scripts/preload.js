const path = require('path')


const destination = path.join(__dirname, "/outputs/")



const {
    contextBridge,
    ipcRenderer
} = require("electron");
const { stat } = require('fs');


window.addEventListener('DOMContentLoaded', () => {
    //set default output path
    document.getElementById("output-destination").value = destination
})


contextBridge.exposeInMainWorld(
    "api", {
        send: (channel, data) => {
            // whitelist channels
            let validChannels = ["async-message", "execute", "select-folder", "open-options-modal"];
            if (validChannels.includes(channel)) {
                ipcRenderer.send(channel, data);
            }
        },
        receive: (channel, func) => {
            let validChannels = ["fromMain", "progress-update", "folder-selected"];
            if (validChannels.includes(channel)) {
                // Deliberately strip event as it includes `sender` 
                ipcRenderer.on(channel, (event, ...args) => func(...args));
            }
        }
    }
);



ipcRenderer.on("conversion-started", (event, index) => {
    var element = document.getElementById("selected-files-list").children[index].getElementsByClassName("file-status-text")[0]
    element.innerHTML = "Converting"
})

ipcRenderer.on("status-update", (event, index, data,) => {
    /*console.log({index, data})
    var element = document.getElementById("selected-files-list").children[index]
    element.innerHTML += "." */
})

ipcRenderer.on("conversion-complete", (event, index) => {
    var element = document.getElementById("selected-files-list").children[index].getElementsByClassName("file-status-text")[0]
    element.innerHTML = "Finished"
})

ipcRenderer.on("folder-selected", (event, path) => {
    document.getElementById("output-destination").value = path
})





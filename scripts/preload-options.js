/*
This file needs:
1. A function that fetches all of the values that have been set
2. The ability to take all values and store them locally
3. Be able to on-startup pre-set all of the selected values to the locally stored ones
*/


const {
    contextBridge,
    ipcRenderer
} = require("electron");


window.addEventListener('DOMContentLoaded', () => {
    /*Fetch and insert all locally saved values here*/
})


contextBridge.exposeInMainWorld(
    "api", {
        send: (channel, data) => {
            // whitelist channels
            let validChannels = ["send-all-args", "send-input-args", "send-output-args"];
            if (validChannels.includes(channel)) {
                ipcRenderer.send(channel, data);
            }
        },
        receive: (channel, func) => {
            let validChannels = ["get-all-args", "get-input-args", "get-output-args"];
            if (validChannels.includes(channel)) {
                // Deliberately strip event as it includes `sender` 
                ipcRenderer.on(channel, (event, ...args) => func(...args));
            }
        }
    }
);


class ArgsObj {
    constructor(inputArgs = [], outputArgs = []) {
        this._inputArgs = inputArgs
        this._outputArgs = outputArgs
    }


    get inputArgs() {
        return this._inputArgs
    }

    get outputArgs() {
        return this._outputArgs
    }

    set inputArgs(inputArr) {
        this._inputArgs = inputArr
    }

    set outputArgs(outputArr) {
        this._outputArgs = outputArr
    }

    getAllArgs() {
        return {"input":this._inputArgs, "output": this._outputArgs}
    }

    addInputArg(arg) {
        this._inputArgs.push(arg)
    }

    addOutputArg(arg) {
        this._outputArgs.push(arg)
    }
}

var argsObj = new ArgsObj()

function constructPreviewScript() {
    let allArgs = argsObj.getAllArgs()
    let tempInput = [...allArgs.input]
    let tempOutput = [...allArgs.output]
    tempInput.splice(tempInput.length, 0, '"input_file"')
    tempOutput.splice(tempOutput.length-1, 0, '"output_file"')
    console.log(tempInput, tempOutput)
    document.getElementById("preview-script").textContent = "ffmpeg " + tempInput.concat(tempOutput).join(" ")
}

/*
Input elements
*/
var overWriteSelect = document.getElementById("overwrite-select")
var frameRateInput = document.getElementById("framerate-input")
var inputElements = [
    overWriteSelect,
]
function assignInputArg() {
    /*
    Fetch value of all elements that assign input arguments
    Then add them to the ArgsObj instance
    should probably use set _inputArgs and assign the entire array instead
    of parsing for which field was changed
    
    This might be suitable as an onchange listener to all related elements
    */
    argsObj._inputArgs = []
    inputElements.forEach((element, index) => {
        if(element.value != "null") {
            argsObj.addInputArg(element.value)
        }
    })
    if(frameRateInput.value > 0){
        argsObj.addInputArg("-r " + frameRateInput.value)
    }
    argsObj.addInputArg("-i")
}

/*
This is no good, find a way to include this with the other elements
*/
frameRateInput.addEventListener("change", function() {
    assignInputArg()
    constructPreviewScript()
})



/*
Output elements
*/
var fileTypeSelect = document.getElementById("file-type-select") //This should be last in outputElements array
var frameRateOutput = document.getElementById("framerate-input")
var outputElements = [
    fileTypeSelect,
]
function assignOutputArg() {
    /*
    Fetch value of all elements that assign output arguments
    Then add them to the ArgsObj instance
    should probably use set _outputArgs and assign the entire array instead
    of parsing for which field was changed
    
    This might be suitable as an onchange listener to all related elements
    */
    argsObj._outputArgs = []
    if(frameRateOutput.value > 0) {
        argsObj.addOutputArg("-r " + frameRateOutput.value)
    }
    
    outputElements.forEach((element, index) => {
        if(element.value != "null") {
            argsObj.addOutputArg(element.value)
        }
    })
    
}

/*
This is no good, find a way to include this with the other elements
*/
frameRateOutput.addEventListener("change", function() {
    assignOutputArg()
    constructPreviewScript()
})


inputElements.forEach((element, index) => {
    element.addEventListener("change", function() {
        assignInputArg()
        constructPreviewScript()
    })
})

outputElements.forEach((element, index) => {
    element.addEventListener("change", function() {
        assignOutputArg()
        constructPreviewScript()
    })
})



/*Init all fields*/
assignInputArg()
assignOutputArg()
constructPreviewScript()

